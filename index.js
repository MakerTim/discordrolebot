fs = require('fs');
require('dotenv').config();
const Discord = require('discord.js');
const bot = new Discord.Client();
const TOKEN = process.env.TOKEN;

const invitesDatabase = require('./database.json');
bot.login(TOKEN);

function saveDatabase() {
    fs.writeFile('./database.json', JSON.stringify(invitesDatabase, null, 4), _ => {
    });
}

function addInviteLink(guild, url, role) {
    return new Promise((resolve, reject) => {
        if (!role.match(/<@&\d{18}>/)) {
            return reject("No Role found at argument 2");
        }

        guild.fetchInvites().then(invites => {
            let foundInvite = null;
            invites.forEach((inviteObject, invite) => {
                if (!foundInvite && (invite === url || 'https://discord.gg/' + invite === url)) {
                    foundInvite = inviteObject;
                }
            });
            if (!foundInvite) {
                return reject("Invite URL not found");
            }

            if (!invitesDatabase[guild.id]) {
                invitesDatabase[guild.id] = {};
            }
            invitesDatabase[guild.id][foundInvite.code] = {
                uses: foundInvite.uses,
                role
            };
            saveDatabase();
            resolve("`" + url + "` => " + role + " added");
        });
    });
}

bot.on('ready', () => {
    console.info(`Logged in as ${bot.user.tag}!`);

    bot.guilds.cache.forEach(guild => {
        const id = guild.id;
        if (!invitesDatabase[id]) {
            invitesDatabase[id] = {};
        }
        saveDatabase();
    });
});

bot.on('message', msg => {
    if (msg.author.bot) return;

    const message = msg.content;
    if (message.startsWith("~")) {
        const args = message.split(" ");
        const command = args.splice(0, 1)[0].substr(1);

        if (command.toLowerCase() === 'invitelink' && msg.member.hasPermission('MANAGE_GUILD')) {
            if (args.length === 2) {
                addInviteLink(msg.guild, args[0], args[1]).then(success => {
                    msg.reply(":white_check_mark: " + success);
                    msg.delete();
                }).catch(error => {
                    msg.reply(error);
                    msg.delete();
                });
            } else {
                msg.reply('Not enough arguments');
                msg.delete();
            }
        }
        if (command.toLowerCase() === 'removeinvitelink' && msg.member.hasPermission('MANAGE_GUILD')) {
            if (args.length === 1) {
                if (args[0] in invitesDatabase[msg.guild.id]) {
                    delete invitesDatabase[msg.guild.id][args[0]];
                    saveDatabase();
                    msg.reply(':ballot_box_with_check: Role ' + args[0] + ' removed');
                } else {
                    msg.reply('Role ' + args[0] + ' not found');
                }
            } else {
                msg.reply('Not enough arguments');
            }
            msg.delete();
        }
        if (command.toLowerCase() === 'invitelinklist' && msg.member.hasPermission('MANAGE_GUILD')) {
            if (args.length === 0) {
                let stringBuilder = '\nRegistered url\'s:\n\n';
                Object.keys(invitesDatabase[msg.guild.id]).forEach(key => {
                    stringBuilder += '`https://discord.gg/' + key + '` => ' + invitesDatabase[msg.guild.id][key].role + '\n';
                });
                msg.reply('' + stringBuilder + '');
            } else {
                msg.reply('Not enough arguments');
            }
            msg.delete();
        }
    }
});

bot.on('guildMemberAdd', member => {
    member.guild.fetchInvites().then(invites => {
        const registeredInvites = invitesDatabase[member.guild.id];

        const invite = invites.find((invite, code) => {
            return (code in registeredInvites) ? invite.uses > registeredInvites[code].uses : false;
        });
        if (!invite) {
            return;
        }
        invitesDatabase[member.guild.id][invite.code].uses = invite.uses;
        saveDatabase();

        const role = invitesDatabase[member.guild.id][invite.code].role.replace("<@&", "").replace(">", "");
        return member.roles.add(member.guild.roles.cache.find(r => r == role));
    });
});
